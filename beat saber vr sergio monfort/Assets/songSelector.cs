using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class songSelector : MonoBehaviour
{

    public GameEvent startSong;
    public GameObject noteSpawner;
    public Song_SO[] songList;
    public Song_SO selectedSong;

    public GameObject textObject;
    public GameObject textObjectHiScore;

    public int index;

    // Start is called before the first frame update
    void Start()
    {
        index = 0;
        loadOptions(index);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnEnable()
    {
        index = 0;
        loadOptions(index);
    }

    public void rightOption()
    {
        if (index+1 > songList.Length-1)
        {
            index = 0;
        }
        else
        {
            index += 1;
        }
        loadOptions(index);
    }

    public void leftOption()
    {
        if (index - 1 < 0)
        {
            index = songList.Length-1;
        }
        else
        {
            index -= 1;
        }
        loadOptions(index);
    }

    public void startOption()
    {
        noteSpawner.GetComponent<noteSpawner>().loadSong(selectedSong);
        Debug.Log("loadeo");
        this.gameObject.SetActive(false);
        //this.gameObject.transform.position = new Vector3(this.gameObject.transform.position.x, this.gameObject.transform.position.y-100, this.gameObject.transform.position.z);
    }

    public void loadOptions(int option)
    {
        selectedSong = songList[option];
        textObject.GetComponent<Text>().text = selectedSong.songName;
        textObjectHiScore.GetComponent<Text>().text = "High Score: "+selectedSong.highScore;
    }

}
