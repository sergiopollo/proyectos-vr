using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class beatSaber : MonoBehaviour
{
    public Material saberMaterial;
    public GameObject filo;

    // Start is called before the first frame update
    void Start()
    {
        if (filo != null)
        {
            filo.GetComponent<MeshRenderer>().material = saberMaterial;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    /*
    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("uevo");
        if (other.transform.tag == "nota")
        {
            if (other.gameObject.GetComponent<MeshRenderer>().material == saberMaterial)
            {
                Debug.Log("bien");
            }
            else
            {
                Debug.Log("mal");
            }
            other.gameObject.SetActive(false);
        }
    }
    */
}
