using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class note : MonoBehaviour
{
    public float moveSpeed;
    public GameEvent activatorTouched;
    public GameEvent addScore;
    public GameObject arrow;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.position += transform.forward * Time.deltaTime * moveSpeed;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "killerWall")
        {
            //this.gameObject.SetActive(false);
            Destroy(this.gameObject);
        }
        else if (other.gameObject.tag == "activator")
        {
            activatorTouched.Raise();
            other.gameObject.SetActive(false);
        }
        else if (other.transform.tag == "filo")
        {
            bool givesPoints = true;

            //primero revisa si es el color correcto
            Material m1 = this.gameObject.GetComponent<MeshRenderer>().material;
            Material m2 = other.transform.parent.GetComponent<beatSaber>().saberMaterial;

            Debug.Log(m1.name);
            Debug.Log(m2.name);

            if (m1 == m2 || m1.name.Contains(m2.name) || m2.name.Contains(m1.name))
            {
                Debug.Log("bien");

            }
            else
            {
                Debug.Log("mal");
                givesPoints = false;
            }

            //luego mira si ha cortado por donde toca
            //other.GetContact(0).point;
            Vector3 collisionPoint = other.ClosestPoint(transform.position);

            float distanceSaberArrow = Vector3.Distance(arrow.transform.position, collisionPoint);
            float distanceArrowCenter = Vector3.Distance(arrow.transform.position, transform.position);
            
            if (distanceArrowCenter < distanceSaberArrow)
            {
                givesPoints = false;
            }
            

            if (givesPoints)
            {
                addScore.Raise();
            }

            //this.gameObject.SetActive(false);
            Destroy(this.gameObject);
        }
    }
}
