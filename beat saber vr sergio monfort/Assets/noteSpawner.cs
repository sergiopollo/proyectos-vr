using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Linq;
using UnityEngine;
using UnityEngine.UI;

public class noteSpawner : MonoBehaviour
{
    public Song_SO selectedSongSO;
    public bool songStarted;
    public float bpm;
    public float beat;
    //private float finalBeat;
    public float time=0;
    //public float timeToHitDelayBarrier = 0;
    public float totalTime = 0;
    public GameObject delayActivatorBarrier;

    public Material[] saberMaterials;

    public GameObject[] notePositions;

    public GameObject note;

    public AudioClip music;

    public GameObject songSelector;

    public GameObject backgroundObjects;

    public GameObject scoreTextObject;
    public int score;



    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        if (this.GetComponent<AudioSource>().clip != null && this.GetComponent<AudioSource>().isPlaying)
        {
            if (!songStarted)
            {
                if (time > beat)
                {
                    time -= beat;
                }
                time += Time.deltaTime;
                
            }

            if (time > beat)
            {
                changeBackgroundColor();
            }

        }

        if (songStarted)
        {
            if (time > beat)
            {
                GameObject newNote = Instantiate(note);
                newNote.transform.position = notePositions[UnityEngine.Random.Range(0, notePositions.Length)].transform.position;
                newNote.GetComponent<MeshRenderer>().material = saberMaterials[UnityEngine.Random.Range(0, saberMaterials.Length)];
                //newNote.transform.Rotate(this.transform.forward);
                newNote.transform.rotation = this.transform.rotation;
                newNote.transform.Rotate(transform.forward, 90* UnityEngine.Random.Range(0,4));
                newNote.GetComponent<note>().moveSpeed = beat*4;
                
                time -= beat;
            }
            time += Time.deltaTime;
            totalTime += Time.deltaTime;
            //if(delayActivatorBarrier.activeSelf) timeToHitDelayBarrier += Time.deltaTime;

        }


        //TEMPORAL
        //scoreTextObject.GetComponent<Text>().text = "Score: " + score;

        //Debug.Log("tt "+ (totalTime)+" l "+ this.GetComponent<AudioSource>().clip.length);
        if (this.GetComponent<AudioSource>().clip != null && totalTime > this.GetComponent<AudioSource>().clip.length)
        //if (!this.GetComponent<AudioSource>().isPlaying)
        {
            songStarted = false;
        }

        if (this.GetComponent<AudioSource>().isPlaying || songStarted)
        {
            scoreTextObject.GetComponent<Text>().text = "Score: " + score;
        }
        else
        {
            scoreTextObject.GetComponent<Text>().text = "select a song";
        }

        if (!this.GetComponent<AudioSource>().isPlaying && !songStarted && songSelector.activeSelf == false)
        //if (!this.GetComponent<AudioSource>().isPlaying && !songStarted && songSelector.transform.position.y < 0)
        {
            songSelector.SetActive(true);
            //songSelector.transform.position = new Vector3(songSelector.transform.position.x, songSelector.transform.position.y + 100, songSelector.transform.position.z);

            if (selectedSongSO != null && score >= selectedSongSO.highScore)
            {
                selectedSongSO.highScore = score;
            }
        }

    }

    public void loadSong(Song_SO song)
    {
        Debug.Log(song.songName);
        selectedSongSO = song;
        bpm = song.bpm;
        beat = (60 / bpm) * 2;
        music = song.songClip;
        //note.GetComponent<note>().moveSpeed = song.noteSpeed;

        //finalBeat = (60 / bpm) * 2;
        if (music != null && this.GetComponent<AudioSource>() != null)
        {
            this.GetComponent<AudioSource>().clip = music;
        }
        //this.GetComponent<AudioSource>().Play();
        songStarted = true;
        time = 0;
        totalTime = 0;
        //timeToHitDelayBarrier = 0;
        delayActivatorBarrier.SetActive(true);
        score = 0;
    }

    public void activatorTouchedEventResponse()
    {
        delayActivatorBarrier.SetActive(false);
        this.GetComponent<AudioSource>().Play();
        //songSelector.gameObject.SetActive(false);
    }

    public void addScore()
    {
        score += 5;
    }

    public void changeBackgroundColor()
    {
        Material mat = backgroundObjects.transform.GetChild(1).GetComponent<MeshRenderer>().material;

        Material m1 = saberMaterials[0];
        Material m2 = saberMaterials[1];

        if (m1 == mat || m1.name.Contains(mat.name) || mat.name.Contains(m1.name))
        {

            changeBackgroundMaterial(m2);

        }
        else
        {
            changeBackgroundMaterial(m1);
        }

    }

    public void changeBackgroundMaterial(Material newMat)
    {
        //backgroundObjects.GetComponent<MeshRenderer>().material = newMat;
        MeshRenderer[] list = backgroundObjects.transform.GetChild(1).GetComponentsInChildren<MeshRenderer>();

        foreach (var mesh in list)
        {
            mesh.material = newMat;
        }

    }

}
