using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class Song_SO : ScriptableObject
{
    public string songName;
    public float bpm;
    public AudioClip songClip;
    public int highScore;
}
