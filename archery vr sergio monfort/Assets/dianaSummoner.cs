using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.GraphicsBuffer;

public class dianaSummoner : MonoBehaviour
{

    public GameObject dianaPrefab;
    public GameObject myDiana;
    public bool goRight;
    public float speed=0.1f;
    public float distance = 10;
    Vector3 destination;
    bool destinationReached=false;
    float direction = 1;

    // Start is called before the first frame update
    void Start()
    {
        if (!goRight)
        {
            direction = direction * -1;
        }
        StartCoroutine(summon());
    }

    // Update is called once per frame
    void Update()
    {
        var step = speed * Time.deltaTime; // calculate distance to move
        

        if (myDiana != null && myDiana.activeSelf)
        {
            if (myDiana.transform.position.x != destination.x && !destinationReached)
            {
                //myDiana.GetComponent<Rigidbody>().MovePosition(new Vector3(myDiana.transform.position.x + speed*direction, myDiana.transform.position.y, myDiana.transform.position.z));
                myDiana.transform.position = Vector3.MoveTowards(myDiana.transform.position, destination, step); 
                if (goRight)
                {
                    if (myDiana.transform.position.x >= destination.x)
                    {
                        destinationReached = true;
                    }
                }
                else
                {
                    if (myDiana.transform.position.x <= destination.x)
                    {
                        destinationReached = true;
                    }
                }
                
                
                
            }
            else
            {
                if (myDiana.transform.position == this.transform.position && destinationReached)
                {
                    destinationReached = false;
                }
                else
                {
                    destinationReached = true;
                    //myDiana.GetComponent<Rigidbody>().MovePosition(new Vector3(myDiana.transform.position.x - speed*direction, myDiana.transform.position.y, myDiana.transform.position.z));
                    myDiana.transform.position = Vector3.MoveTowards(myDiana.transform.position, this.transform.position, step);
                }

            }

        }
        /*
        if (myDiana != null && myDiana.activeSelf)
        {
            if (myDiana.transform.position.x != destination.x && !destinationReached)
            {
                //myDiana.GetComponent<Rigidbody>().MovePosition(new Vector3(myDiana.transform.position.x + speed*direction, myDiana.transform.position.y, myDiana.transform.position.z));
                myDiana.GetComponent<Rigidbody>().MovePosition(new Vector3(myDiana.transform.position.x + speed * direction, myDiana.transform.position.y, myDiana.transform.position.z));
                if (goRight)
                {
                    if (myDiana.transform.position.x >= destination.x)
                    {
                        destinationReached = true;
                    }
                }
                else
                {
                    if (myDiana.transform.position.x <= destination.x)
                    {
                        destinationReached = true;
                    }
                }



            }
            else
            {
                if (myDiana.transform.position == this.transform.position && destinationReached)
                {
                    destinationReached = false;
                }
                else
                {
                    destinationReached = true;
                    myDiana.GetComponent<Rigidbody>().MovePosition(new Vector3(myDiana.transform.position.x - speed * direction, myDiana.transform.position.y, myDiana.transform.position.z));
                }

            }

        }
        */
    }

    IEnumerator summon()
    {
        while (true)
        {
            if (myDiana == null)
            {
                GameObject newDiana = Instantiate(dianaPrefab);
                myDiana = newDiana;
                myDiana.transform.position = this.transform.position;
                myDiana.transform.Rotate(0, 90, 0);
                //StartCoroutine(move());
                this.destination = new Vector3(myDiana.transform.position.x + distance * direction, myDiana.transform.position.y, myDiana.transform.position.z);
                
            }

            if (!myDiana.activeSelf)
            {
                myDiana.transform.position = this.transform.position;
                this.destination = new Vector3(myDiana.transform.position.x + distance * direction, myDiana.transform.position.y, myDiana.transform.position.z);
                myDiana.SetActive(true);
            }

            yield return new WaitForSeconds(1);
        }
    }
    /*
    IEnumerator move()
    {
        bool go = true;
        bool destinationReached = false;
        Vector3 destination = new Vector3(myDiana.transform.position.x + 10, myDiana.transform.position.y, myDiana.transform.position.z);









        
        while (go)
        {
            if (myDiana == null || !myDiana.activeSelf)
            {
                go = false;
                break;
            }
            else
            {
                if(myDiana.transform.position.x < destination.x && !destinationReached)
                {
                    myDiana.GetComponent<Rigidbody>().MovePosition(new Vector3(myDiana.transform.position.x + 0.1f, myDiana.transform.position.y, myDiana.transform.position.z));
                    if (myDiana.transform.position.x >= destination.x)
                    {
                        destinationReached = true;
                    }
                }
                else
                {
                    if (myDiana.transform.position == this.transform.position && destinationReached)
                    {
                        destinationReached = false;
                    }
                    else
                    {
                        destinationReached = true;
                        myDiana.GetComponent<Rigidbody>().MovePosition(new Vector3(myDiana.transform.position.x - 0.1f, myDiana.transform.position.y, myDiana.transform.position.z));
                    }
                    
                }
                
            }
              
            yield return new WaitForSeconds(0.01f);
        }
    
    }
    */
}
