using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class arrowController : MonoBehaviour
{

    public bool isFired = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (isFired)
        {

            float velX = this.GetComponent<Rigidbody>().velocity.x;
            float velY = this.GetComponent<Rigidbody>().velocity.y;
            float velZ = this.GetComponent<Rigidbody>().velocity.z;
            float combinedVel = Mathf.Sqrt(velX * velX + velZ * velZ);
            float fallAngle = Mathf.Atan2(velY, combinedVel)*180/Mathf.PI;

            this.transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, -fallAngle);


            /*Debug.Log("vel: "+ this.GetComponent<Rigidbody>().velocity);
            Quaternion rot = transform.rotation;
            rot.SetLookRotation(this.GetComponent<Rigidbody>().velocity);
            transform.rotation = rot;*/
            //transform.rotation.SetLookRotation(this.GetComponent<Rigidbody>().velocity);
            //transform.rotation = Quaternion.LookRotation(this.GetComponent<Rigidbody>().velocity);




            //this.transform.forward = Vector3.Slerp(this.transform.forward, this.GetComponent<Rigidbody>().velocity.normalized, Time.deltaTime);
        }
    }
}
