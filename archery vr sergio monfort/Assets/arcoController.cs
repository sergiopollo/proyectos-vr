using JetBrains.Annotations;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;

public class arcoController : MonoBehaviour
{
    public GameObject arrowPrefab;
    public GameObject stringGrabObject;
    public GameObject myArrow;
    public float arrowForce;
    public bool isAiming = false;
    public TextMeshPro text;
    public gameManager manager;

    // Start is called before the first frame update
    void Start()
    {
        manager = FindObjectsOfType<gameManager>()[0];
    }

    // Update is called once per frame
    void Update()
    {
        if (isAiming)
        {
            //calculo de fuerza de la flecha
            Vector3 startBowPosition = this.transform.position;
            Vector3 endBowPosition = stringGrabObject.transform.position;
            float distance = Vector3.Distance(startBowPosition, endBowPosition);

            float minDistance = 0.144f;
            float maxDistance = 0.284f;
            arrowForce = Mathf.InverseLerp(minDistance, maxDistance, distance);

            Debug.Log("force: " + arrowForce);



            //this.GetComponent<TrajectoryPredictor>().PredictTrajectory(ProjectileData());


        }
    }

    ProjectileProperties ProjectileData()
    {
        ProjectileProperties properties = new ProjectileProperties();
        Rigidbody r = myArrow.GetComponent<Rigidbody>();

        properties.direction = myArrow.transform.forward;
        properties.initialPosition = myArrow.transform.position;
        properties.initialSpeed = arrowForce;
        properties.mass = r.mass;
        properties.drag = r.drag;

        return properties;
    }


    public void fireArrow()
    {
        if (myArrow != null)
        {
            //Debug.Log("fire arrow");
            myArrow.transform.parent = null;
            myArrow.GetComponent<Rigidbody>().useGravity = true;
            
            myArrow.GetComponent<arrowController>().isFired = true;
            myArrow.GetComponent<Rigidbody>().AddRelativeForce(new Vector3(-arrowForce*10-5,0,0), ForceMode.Impulse);
            //StartCoroutine(triggerDelay());
            myArrow.GetComponent<arrowController>().enabled = true;
            this.isAiming = false;
        }

    }

    public void spawnArrow()
    {
        GameObject newArrow = Instantiate(arrowPrefab);
        myArrow = newArrow;
        myArrow.GetComponent<arrowController>().enabled = true;
        myArrow.transform.parent = stringGrabObject.transform;
        myArrow.transform.position = stringGrabObject.transform.position;
        myArrow.transform.rotation = stringGrabObject.transform.rotation;
        myArrow.GetComponent<Rigidbody>().useGravity = false;
        //test
        //DrawTrajectory();
        
        this.isAiming = true;
    }

    public IEnumerator triggerDelay()
    {
        GameObject thisArrow = myArrow;
        myArrow = null;
        yield return new WaitForSeconds(0.04f);
        thisArrow.GetComponent<CapsuleCollider>().isTrigger = false;
    }


    public void addPoints()
    {
        text.SetText(""+manager.points);
    }


    /*
    public void DrawTrajectory()
    {
        LineRenderer myLineRenderer = this.GetComponent<LineRenderer>();
        int linePoints = 175;

        Vector3 origin = this.transform.position;
        Vector3 startVelocity = 10f * this.transform.up;
        myLineRenderer.positionCount = linePoints;
        float time = 0;
        for (int i = 0; i < linePoints; i++)
        {
            var x = (startVelocity.x * time) + (Physics.gravity.x / 2 * time * time);
            var y = (startVelocity.y * time) + (Physics.gravity.y / 2 * time * time);
            Vector3 point = new Vector3(x, y, 0);
            myLineRenderer.SetPosition(i, origin + point);
            time += 0.01f;
        }


    }
    */
}
