using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using static Unity.VisualScripting.Metadata;

public class gameManager : MonoBehaviour
{
    public GameEvent resetBolosEvent;
    public GameObject ball;
    public int puntuacion;
    public GameObject pizarra;
    public List<int> puntosPorTurno = new List<int>();
    public int turnos = 21;
    public bool gameOver = false;
    public GameObject bolos;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        //para testing con teclado y raton
        /*
        if (Input.GetButtonDown("Fire1"))
        {
            Debug.Log("muevebola");
            ball.GetComponent<Rigidbody>().AddForce(new Vector3(0,0,10));
        }

        if (Input.GetButtonDown("Fire2"))
        {
            Debug.Log("resetbolos");
            resetBolos();
        }
        */
    }

    public void resetScene()
    {
        SceneManager.LoadScene("SampleScene");
    }

    public void resetBolos()
    {
        resetBolosEvent.Raise();
    }

    public void addPoints()
    {
        if (!gameOver)
        {
            puntuacion++;
            
        }
    }

    private void updateScore()
    {
        //pone la puntuacion bonita segun las reglas de los bolos
        string scoreBoard = "";
        for (int i = 0; i < 21; i++)
        {
            Debug.Log("puntosPorTurnocount:" + puntosPorTurno.Count);
            Debug.Log("i:" + i);
            if (puntosPorTurno.Count > i)
            {
                Debug.Log("entro1");
                int thisPoints = puntosPorTurno[i];
                if (i%2 != 0 && i != 21)
                {
                    Debug.Log("entro2");
                    int lastTurnPoints = puntosPorTurno[i - 1];
                    Debug.Log("lastTurnPoints:" + lastTurnPoints);
                    if (lastTurnPoints == 10)
                    {
                        scoreBoard += "| X |   |";
                    }
                    else if (lastTurnPoints+thisPoints == 10)
                    {
                        scoreBoard += "| " + lastTurnPoints + " | / |";
                    }
                    else
                    {
                        scoreBoard += "| " + lastTurnPoints + " | " + thisPoints + " |";
                    }
                }else if (i%2 == 0 && puntosPorTurno.Count-1 == i)
                {
                    scoreBoard += "| " + thisPoints + " |   |";
                }
            }
            else
            {
                scoreBoard += "|   |   |";
            }
        }



        pizarra.GetComponentInChildren<TextMeshProUGUI>().text = scoreBoard;
    }

    public void endTurn()
    {
        StartCoroutine(corrutinaPararBolos());
        StartCoroutine(corrutinaForEndTurn());
    }

    public IEnumerator corrutinaForEndTurn()
    {
        //comprueba que no haya bolos a�n cayendo cuando la bola llega al final para contar bien la puntuaci�n
        bool acaba = false;
        while (!acaba) {
            for (int i = 0; i < bolos.transform.childCount; ++i)
            {
                acaba = true;
                if (bolos.transform.GetChild(i).GetComponent<Rigidbody>().velocity != Vector3.zero || bolos.transform.GetChild(i).GetComponent<Rigidbody>().angularVelocity != Vector3.zero)
                {
                    acaba = false;
                }
            }
            yield return new WaitForSeconds(1f);
        }
        if (!gameOver)
        {
            puntosPorTurno.Add(puntuacion);
            if (puntuacion == 10)
            {
                puntosPorTurno.Add(0);
                turnos--;
            }
            puntuacion = 0;

            turnos--;

            updateScore();
            if (turnos%2 !=0)
            {
                resetBolos();
            }
            
            if (turnos == 0)
            {
                gameOver = true;
            }
        }
    }

    public IEnumerator corrutinaPararBolos()
    {
        //para los bolos
        yield return new WaitForSeconds(5f);
        
        for (int i = 0; i < bolos.transform.childCount; ++i)
        {
            bolos.transform.GetChild(i).GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
                
        }

        for (int i = 0; i < bolos.transform.childCount; ++i)
        {
            bolos.transform.GetChild(i).GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
            bolos.transform.GetChild(i).GetComponent<Rigidbody>().WakeUp();
        }

    }

}
