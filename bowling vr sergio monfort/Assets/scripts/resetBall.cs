using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class resetBall : MonoBehaviour
{
    public GameEvent endTurn;
    public GameObject ballGiver;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "ball")
        {
            other.gameObject.transform.position = new Vector3(ballGiver.transform.position.x, ballGiver.transform.position.y + 0.5f, ballGiver.transform.position.z);
            endTurn.Raise();
        }
    }

}
