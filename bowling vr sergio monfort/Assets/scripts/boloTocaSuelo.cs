using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class boloTocaSuelo : MonoBehaviour
{
    public GameEvent addPoints;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "suelo")
        {
            addPoints.Raise();
            this.GetComponentInParent<AudioSource>().Play();
            this.gameObject.SetActive(false);
        }
    }
}
