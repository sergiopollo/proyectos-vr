using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class boloManager : MonoBehaviour
{
    private Vector3 boloInitialPosition;
    private Vector3 boloInitialRotation;
    public GameObject tocaSuelo;
    // Start is called before the first frame update
    void Start()
    {
        boloInitialPosition = this.transform.position;
        boloInitialRotation = this.transform.eulerAngles;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void resetMyPosition()
    {
        this.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
        this.GetComponent<Rigidbody>().WakeUp();
        this.GetComponent<Rigidbody>().velocity = Vector3.zero;
        this.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
        this.transform.position = boloInitialPosition;
        this.transform.eulerAngles = boloInitialRotation;
        tocaSuelo.SetActive(true);
    }

}
