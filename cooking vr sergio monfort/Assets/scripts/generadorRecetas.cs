using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class generadorRecetas : MonoBehaviour
{

    public List<GameObject> ingredients;
    public List<GameObject> boxes;
    public GameObject destino;
    public List<foodItem> comanda;
    public GameObject pizarra;
    public GameObject pizarraPuntuacion;
    public int puntuacion;
    public AudioClip soundCorrect;
    public AudioClip soundWrong;
    

    // Start is called before the first frame update
    void Start()
    {
        //poner imagenes chorras
        imagenesIngredientesEnCajas();
        newOrder();
        updateScore(0);
        spawnIngredient();
    }

    // Update is called once per frame
    void Update()
    {
        //descomentar para controles con mouse
        /*
        if (Input.GetButtonDown("Fire1"))
        {
            spawnIngredient();

        }

        if (Input.GetButtonDown("Fire2"))
        {
            sendOrder();
        }
        */
    }


    private void imagenesIngredientesEnCajas()
    {
        for (int i = 0; i < ingredients.Count; i++)
        {
            GameObject NewObj = new GameObject(); //Create the GameObject
            Image NewImage = NewObj.AddComponent<Image>(); //Add the Image Component script
            NewImage.sprite = ingredients[i].GetComponent<foodItem>().foodImage; //Set the Sprite of the Image Component on the new GameObject
            NewObj.GetComponent<RectTransform>().SetParent(boxes[i].transform.GetChild(0).transform); //Assign the newly created Image GameObject as a Child of the Parent Panel.
            NewObj.transform.position = boxes[i].transform.GetChild(0).transform.position;
            NewObj.transform.localScale = new Vector3(3f, 3f, 3f);
            NewObj.SetActive(true); //Activate the GameObject
        }
    }

    public void spawnIngredient()
    {
        Debug.Log("comida");

        for (int i = 0; i < ingredients.Count; i++)
        {
            GameObject obj = Instantiate(ingredients[i]);
            obj.transform.position = new Vector3(boxes[i].gameObject.transform.position.x, boxes[i].gameObject.transform.position.y + 1, boxes[i].gameObject.transform.position.z);
        }
    }
    
    private void OnDrawGizmos()
    {
        //para ver el area de la hitbox donde se entregan los ingredientes (solo para editor)
        Gizmos.color = Color.red;
        Gizmos.DrawWireCube(destino.transform.position, new Vector3(destino.transform.localScale.x/2, destino.transform.localScale.y * 2, destino.transform.localScale.z * 2) * 2);
    }

    public void sendOrder()
    {
        Debug.Log("enviando comanda");

        //coje todos los colliders que estan colisionando encima de "caja destino"
        Collider[] hitColliders = Physics.OverlapBox(destino.transform.position, new Vector3(destino.transform.localScale.x/2, destino.transform.localScale.y*2, destino.transform.localScale.z*2), Quaternion.identity);
        List<foodItem> foodItemsList = new List<foodItem>();

        foreach (Collider collider in hitColliders)
        {

            GameObject foodItemGameObject = collider.gameObject;
            if (foodItemGameObject.GetComponent<foodItem>())
            {
                foodItemsList.Add(foodItemGameObject.GetComponent<foodItem>());
                Debug.Log("item: " + foodItemGameObject.GetComponent<foodItem>().foodName);
            }
            
        }

        //comprueba que la comanda sea correcta:
        comanda.Sort(SortByFoodName);
        foodItemsList.Sort(SortByFoodName);

        //print de las listas
        ///////////////////////////
        string somethingAAlist = "";

        foreach (foodItem i in comanda)
        {
            somethingAAlist += " item: " + i.foodName;
        }

        Debug.Log("comanda: " + somethingAAlist);

        string somethinglist = "";

        foreach (foodItem i in foodItemsList)
        {
            somethinglist += " item: " +i.foodName;
        }

        Debug.Log("fooditemlist: "+somethinglist);
        ////////////////////////////


        //compare strings
        List<string> comandaStrings = new List<string>();
        
        foreach (foodItem i in comanda)
        {
            comandaStrings.Add(i.foodName);
        }

        List<string> foodItemsListStrings = new List<string>();

        foreach (foodItem i in foodItemsList)
        {
            foodItemsListStrings.Add(i.foodName);
        }

        if (comandaStrings.SequenceEqual(foodItemsListStrings))
        {
            Debug.Log("correct order!");
            this.GetComponent<AudioSource>().clip = soundCorrect;
            this.GetComponent<AudioSource>().Play();    
            foreach (foodItem i in foodItemsList)
            {
                Destroy(i.gameObject);
            }
            //borrar imagenes
            var canvas = pizarra.transform.GetChild(0);

            for (int i = canvas.transform.childCount - 1; i > 0; i--)
            {
                Destroy(canvas.transform.GetChild(i).gameObject);
            }
            updateScore(10);
            newOrder();
        }
        else
        {
            Debug.Log("WRONG order!");
            this.GetComponent<AudioSource>().clip = soundWrong;
            this.GetComponent<AudioSource>().Play();
            if (foodItemsList.Count != 0) {
                updateScore(-3);
            }
        }
    }

    private void updateScore(int scorepoints)
    {
        puntuacion += scorepoints;

        pizarraPuntuacion.GetComponentInChildren<TextMeshProUGUI>().text = "$ " + puntuacion;
    }

    private static int SortByFoodName(foodItem o1, foodItem o2)
    {
        return o1.foodName.CompareTo(o2.foodName);
    }
    /*
    public IEnumerator comprobarIngredientes()
    {
        while (true)
        {
            yield return new WaitForSeconds(1);
            //RaycastHit[] hit = Physics.SphereCastAll();

        }
    } 
    */
    private void newOrder()
    {
        Debug.Log("nueva comanda");
        comanda.Clear();
        var numIngredients = UnityEngine.Random.Range(2, 4);

        for (int i = 0; i < numIngredients; i++)
        {
            var posIngredient = UnityEngine.Random.Range(0, 3);
            comanda.Add(ingredients[posIngredient].GetComponent<foodItem>());

        }

        var stringComanda = "";

        for (int i = 0; i < comanda.Count; i++)
        {
            stringComanda += ", "+comanda[i].GetComponent<foodItem>().foodName;

        }

        Debug.Log("comanda actual: " + stringComanda);

        

        for (int i = 0; i < comanda.Count; i++)
        {


            GameObject NewObj = new GameObject(); //Create the GameObject
            Image NewImage = NewObj.AddComponent<Image>(); //Add the Image Component script
            NewImage.sprite = comanda[i].GetComponent<foodItem>().foodImage; //Set the Sprite of the Image Component on the new GameObject
            NewObj.GetComponent<RectTransform>().SetParent(pizarra.transform.GetChild(0).transform); //Assign the newly created Image GameObject as a Child of the Parent Panel.
            NewObj.transform.position = new Vector3(pizarra.transform.position.x+2-2*i, pizarra.transform.position.y-0.5f, pizarra.transform.position.z+0.1f);
            NewObj.transform.localScale = new Vector3(1f, 1f, 1f);
            NewObj.SetActive(true); //Activate the GameObject


        }


        pizarra.GetComponentInChildren<TextMeshProUGUI>().text = "comanda actual: "+stringComanda; 


        this.GetComponent<AudioSource>().Play();    

    }


}
